// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA
// Copyright (C) 2009-2010 - DIGITEO
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

pendulum_path = get_absolute_file_path('mbdyn_pendulum.sce');

xcos(pendulum_path + "/Pendulum.cosf");

